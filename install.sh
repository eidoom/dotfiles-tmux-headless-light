#!/usr/bin/env bash

here=$(realpath "$0" | xargs dirname)

raw=tmux.conf
cfg=.tmux.conf

cp "$here/$raw" "$here/$cfg"
echo -e "run-shell $here/tmux-sensible/sensible.tmux\n$(cat "$here/$cfg")" > "$here/$cfg"
echo -e "run-shell $here/tmux-pain-control/pain_control.tmux\n$(cat "$here/$cfg")" > "$here/$cfg"

ln -s "$here/$cfg" ~/"$cfg"
