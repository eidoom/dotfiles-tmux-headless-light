# dotfiles-tmux-headless-light

## Description

My installation script and configuration files for `tmux` when used remotely on a system where you don't have administrative privileges.
No system pacakges are installed, so relies on existing installation of `tmux`.
[tpm](https://github.com/tmux-plugins/tpm) is not used, to support old versions of `tmux`.
`Ctrl`+`a` is used as prefix so as not to conflict with local prefix, `Ctrl`+`b`, when remote `tmux` session (over `ssh`) is used within a local one.
Tested on `tmux` version 1.8.

Uses `tmux` plugins
* [sensible](https://github.com/tmux-plugins/tmux-sensible)
* [pain control](https://github.com/tmux-plugins/tmux-pain-control)

## Installation

* Can be installed to any directory. I favour `$HOME`
```shell
cd
```
* Install with 
	* SSH clone (if you're me)
	```shell
	git clone --depth 1 --recurse-submodules git@gitlab.com:eidoom/dotfiles-tmux-headless-light.git .dotfiles-tmux-headless-light
	```
	* or HTTPS clone (if you're not me)
	```shell
	git clone --depth 1 --recurse-submodules https://gitlab.com/eidoom/dotfiles-tmux-headless-light.git .dotfiles-tmux-headless-light
	```
* Delete the old tmux configuration
```shell
rm ~/.tmux.conf
```
* Run the installation script
```shell
./.dotfiles-tmux-headless-light/install.sh
```

## Using tmux with this configuration

* `prefix`=`Ctrl`+`a`.
* Switch panes with `prefix`+`<directions>`, where `<direction>` are arrow keys or `vim` direction keys, then `Esc`.
* Switch windows with `prefix`+`<num>`, where `<num>` is the index of the window.
* Create panes with `prefix`+`-` for vertical split or `prefix`+`\` for horizontal split.
